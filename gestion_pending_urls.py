from urllib.parse import urljoin

from gestion_logs import addRowInLogs


def addRowInPendingURLs(database, soup, link) :
    # function addRowInPendingURLs qui permet d'ajouter les nouvelles urls dans la collection pendingURLs
    # @params database, soup, link

    collection_in = database['pendingURLs']
    # ajout dans collection_in des nouvelles Url
    new_links = soup.find_all('a', href=True)

    for new_link in new_links:
        href = new_link['href']

        href = urljoin(link["url"], href)

        # ne gerer que 100 pages par scope
        nbLink = collection_in.count_documents({'scope': link["scope"]})
        if nbLink < 100:
            document = {
                'url': href,
                'scope': link["scope"],
                'status': 'pending',
                'start_date': '',
                'stop_date': '',
                'next_date':'',
                'retried': 0
            }
            result = collection_in.update_one({'url': href}, {"$setOnInsert": document}, upsert=True)
            if result.upserted_id:
                addRowInLogs(database,'trace', f"élément {result.upserted_id} inséré dans la collection pendingURLs")
                print(f"URL {href} insérée dans la collection pendingURLs avec {result.upserted_id}")
            else:
                addRowInLogs(database,'warning', f"URL déjà présente dans la collection pendingURLs : {href}")
                print(f"URL {href} déjà présente dans la collection pendingURLs")
        else:
            addRowInLogs(database,'warning', f"limite de pages atteinte pour {link['scope']}")
            print(f"limite de pages atteinte pour {link['scope']}")
