import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

def DBConnection() :
    # Connexion à la base de données MongoDB
    client = MongoClient('mongodb://localhost:27017/')
    database = client['seo']
    return database
