from config import scope
from traitement_global import addRowInLogs

def traitement_premier_url(database):
    # function traitement_premier_url qui permet d'ajouter la premiere url définie dans le fichier de config
    # dans la collection pendingURLs
    # @params database
    collection = database['pendingURLs']

    start_url = scope

    try:
        document = {
            'url': start_url,
            'scope': scope,
            'status': 'pending',
            'start_date':'',
            'stop_date': '',
            'next_date':'',
            'retried': 0
        }
        res = collection.insert_one(document)
        addRowInLogs(database,'trace', f"élément {res.inserted_id} inséré dans la collection pendingURLs")
        print(f"URL insérée dans la collection logs: {start_url} avec id {res.inserted_id}")

    except Exception as e:
        addRowInLogs(database, 'warning', f"URL déjà présente dans la collection scrapedData : {start_url}")
        print(f"URL déjà présente dans la collection logs: {start_url}")




