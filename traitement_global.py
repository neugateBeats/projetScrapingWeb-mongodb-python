import time
from datetime import datetime, timedelta

import requests
from bs4 import BeautifulSoup
from gestion_logs import addRowInLogs
from gestion_scraped_data import addRowInScrapedData
from gestion_pending_urls import addRowInPendingURLs


def fini(database) :
    # function fini qui renvoie True si ne reste plus d'élément à traiter
    # et False dans le cas contraire
    # @params database
    collection_in = database['pendingURLs']
    myquery = {'$or': [{'status': 'pending'}
        , {'status': 'in_progress'}
        , {'status': 'to_retry', 'retried': {'$lt': 5}}]}
    return collection_in.count_documents(myquery) == 0


def traitement_global(database) :
    # function traitement_global qui permet d'ajouter les urls dans la collection pendingURLs
    # et de les traiter en ajoutant les documents dans la collection scrapedData
    # @params database
    collection_in = database['pendingURLs']

    while not fini(database):
        # cherche un element pending ou in progress et démarré depuis plus de 60 secondes
        # ou qui doivent être réessayé
        myquery = {'$or': [{'status': 'pending'}
            , {'status': 'in_progress', 'start_date': {'$lte': datetime.now() - timedelta(seconds=60)}}
            , {'status': 'to_retry', 'next_date': {'$lte': datetime.now()}, 'retried': {'$lt': 5}}]}

        # update cet element en mettant son status à in_progress et start_date à la date de maintenant
        update = {"$set": {"status": "in_progress", 'start_date': datetime.now()}, "$inc": {"retried": 1}}

        # find 1 et update
        link = collection_in.find_one_and_update(myquery, update)

        if link is None:
            time.sleep(30)

        else:
            print(link)
            addRowInLogs(database,'trace', f"élément {link['_id']} mis à jour avec {update}")

            # Url de la page à analyser
            url = link['url']
            print(url)
            try:
                response = requests.get(url)

                # Vérifiez si la requête a réussi (statut 200)
                if response.status_code == 200:
                    # Utilise BeautifulSoup pour analyser le contenu HTML de la page
                    soup = BeautifulSoup(response.text, 'html.parser')

                    # ajout dans ScrapedData
                    addRowInScrapedData(database, soup, url, response.text)

                    # ajout dans PendingURLs des nouvelles Url
                    addRowInPendingURLs(database, soup, link)

                    # met le status à completed
                    newvalues = {"$set": {"status": "completed", 'stop_date': datetime.now()}}
                    log_type = "trace"
                else:
                    # la reponse n'est pas 200, il faudra réessayer plus tard
                    print(f"Failed to retrieve the page. Status code: {response.status_code}")
                    # met le status à to_retry
                    newvalues = {"$set": {"status": "to_retry", "next_date": datetime.now() + timedelta(seconds=30)}}
                    log_type = "error"

                myquery = {"_id": link['_id']}
                collection_in.update_one(myquery, newvalues)
                addRowInLogs(database, log_type, f"élément {link['_id']} mis à jour avec {newvalues}")


            except Exception as e:
                addRowInLogs(database, 'error', f"Erreur lors de la récupération de l'URL {url}")
                print(f"Erreur lors de la récupération de l'URL {url}: {e}")
