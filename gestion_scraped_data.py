from datetime import datetime

from gestion_logs import addRowInLogs


def addRowInScrapedData(database, soup, url, html_text):
    # function addRowInScrapedData qui permet d'ajouter les nouveaux documents scrappés dans la collection scrapedData
    # @params database, soup, url, html_text

    collection_out = database['scrapedData']
    documentScraped = {
        'url': url,
        'date': datetime.now(),
        'html': html_text,
        'title': '',
        'h1': [],
        'h2': [],
        'h3': [],
        'h4': [],
        'em': [],
        'strong': [],
        'b': []
    }

    # Extraire la balise <title>
    title_tag = soup.title
    if title_tag:
        print(f"Title: {title_tag.text.strip()}")
    documentScraped['title'] = title_tag.text.strip()

    tab_h1 = []
    tab_h2 = []
    tab_h3 = []
    tab_h4 = []
    tab_b = []
    tab_strong = []
    tab_em = []

    # Extraire les balises <h1>, <h2> <h3>, <h4>
    header_tags = soup.find_all(['h1', 'h2', 'h3','h4'])
    for header_tag in header_tags:
        print(f"{header_tag.name}: {header_tag.text.strip()}")
        if header_tag.name == "h1":
            tab_h1.append(header_tag.text.strip())
        if header_tag.name == "h2":
            tab_h2.append(header_tag.text.strip())
        if header_tag.name == "h3":
            tab_h3.append(header_tag.text.strip())
        if header_tag.name == "h4":
            tab_h4.append(header_tag.text.strip())

    documentScraped['h1'] = tab_h1
    documentScraped['h2'] = tab_h2
    documentScraped['h3'] = tab_h3
    documentScraped['h4'] = tab_h4

    # Extraire la balise <b>
    bold_tags = soup.find_all('b')
    for bold_tag in bold_tags:
        print(f"{bold_tag.name}: {bold_tag.text.strip()}")
        tab_b.append(bold_tag.text.strip())
    documentScraped['b'] = tab_b

    # Extraire la balise <strong>
    strong_tags = soup.find_all('strong')
    for strong_tag in strong_tags:
        print(f"{strong_tag.name}: {strong_tag.text.strip()}")
        tab_strong.append(strong_tag.text.strip())
    documentScraped['strong'] = tab_strong

    # Extraire la balise <em>
    italic_tags = soup.find_all('em')
    for italic_tag in italic_tags:
        print(f"{italic_tag.name}: {italic_tag.text.strip()}")
        tab_em.append(italic_tag.text.strip())
    documentScraped['em'] = tab_em

    # ajout le document à la collection s'il n'existe pas déjà
    result = collection_out.update_one({'url': url}, {"$setOnInsert": documentScraped}, upsert=True)
    if result.upserted_id:
        addRowInLogs(database,'trace', f"document de l'url {url} insérée dans la collection scrapedData avec {result.upserted_id}")
        print(f"document de l'url {url} insérée dans la collection scrapedData avec {result.upserted_id}")
    else:
        addRowInLogs(database,'warning', f"URL déjà présente dans la collection scrapedData : {url}")
        print(f"URL déjà présente dans la collection scrapedData: {url}")
