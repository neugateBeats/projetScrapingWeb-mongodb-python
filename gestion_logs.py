from datetime import datetime


def addRowInLogs(database, type, info) :
    # function addRowInLogs qui permet d'ajouter des logs dans la collection logs
    # @params database, type, info

    collection_log = database['logs']
    try:
        document = {
            'date': datetime.now(),
            'type': type,
            'info': info
        }
        res = collection_log.insert_one(document)

    except Exception as e:
        print(f"Exception au moment de l'insertion dans la collection logs : {e}")

