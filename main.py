from db_connection import DBConnection
from traitement_global import traitement_global
from traitement_premiere_url import traitement_premier_url

def main():

    database = DBConnection()

    # ajout de la premiere url définie dans le fichier de config
    traitement_premier_url(database)
    # ajout des urls dans la collection pendingURLs
    # et traitement des urls en ajoutant les documents dans la collection scrapedData
    traitement_global(database)

if __name__ == "__main__":
    main()
